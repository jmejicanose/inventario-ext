var datosFormulario; //lo que se va a enviar
var nombre;
var tipo;
var vencimiento;
$(function(){

	$("input:submit").click(function() {
		datosFormulario= $('#formUpdate');
		nombre = document.getElementById('nombre').value;
		tipo = document.getElementById('tipo').value;
		vencimiento = document.getElementById('vencimiento').value;
		if(cantidad== "" || nombre == "" || vencimiento ==""){
			mostrarAviso(2);
			return false;
		}
		obtenerMensaje();
		return false;
	});

});
function mostrarAviso(error){
	switch(error) {
	    case 1:
	        $("#aviso").html("<div class='alert alert-danger alert-dismissable'><button type='button' class='close'"
		 	+"data-dismiss='alert' aria-hidden='true'>&times;</button>El contrato ya existe!.</div>");
	        break;
	    case 2:
	        $("#aviso").html("<div class='alert alert-danger alert-dismissable'><button type='button' class='close'"
		 	+"data-dismiss='alert' aria-hidden='true'>&times;</button>Ambos campos son necesarios!</div>");
	        break;	
	}
}
function obtenerMensaje() {
	var action=document.getElementById('formUpdate').action;
	$.ajax({
        url: action,
        type: 'POST',
        data: datosFormulario.serialize(),
        success : function(data) {
			var arreglo=Object.values(data);
			console.log(arreglo[1])
        	if(arreglo[1]==3) window.location.replace("/contratos");
            mostrarAviso(arreglo[1]);
        }
    });
}