var formularioDistribuido;
// funcion que agrega las nuevas filas a la tabla
function agregarFilas(productos) {
    for (var i = 0; productos[i]; i++) {
        var producto = productos[i],
            nombre = producto.nombreProducto,
            codigo = producto.codigo,
            cantidad = producto.cantidadDistribuido,
            idAlmacen = producto.id;

        getFilas(nombre, codigo, cantidad, idAlmacen);
    }
    $('#dataTables-example').DataTable().draw();
}

function getFilas(nombre, codigo, cantidad, idAlmacen) {
    var table = $('#dataTables-example').DataTable();
    var string1, string2, string3;
    // llenar el string 
    string1 = '<td>' + nombre + '</td>';
    string2 = '<td>' + codigo + '</td>';
    string3 = '<p name="' + idAlmacen + '">' + cantidad + '</p>';

    table.row.add([
        string2,
        string1,
        string3
    ]);
}

// elimina todas las filas de la tabla, menos la principal
function eliminaFilas() {
    //$("#tbodyid").empty();    
    $('#dataTables-example').DataTable().clear().draw();
};

// obtencion de los datos para el top ten
function obtenerDistribuido() {
    $.ajax({
        url: '/distribuido',
        type: 'POST',
        data: formularioDistribuido.serialize(),
        success: function (data) {
            // Almacen
            agregarFilas(data);
        }
    });
}


// obtencion de los datos para el top ten
function reiniciarDistribuido() {
    $.ajax({
        url: '/distribuido',
        type: 'POST',
        data: formularioDistribuido.serialize(),
        success: function (data) {
            // Almacen
            eliminaFilas();
            agregarFilas(data);
        }
    });
}

// funcion principal
$(function () {
    // obtengo el formulario del almacen
    formularioDistribuido = $('#formdistribuido');
    obtenerDistribuido();

    // select de sucursal
    $("select[name=plaza]").change(function () {
        reiniciarDistribuido();
    });

    // select de categoría
    $("select[name=categoria]").change(function () {
        reiniciarDistribuido();
    });
});
