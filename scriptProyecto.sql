/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='NO_ENGINE_SUBSTITUTION' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;

DROP DATABASE IF EXISTS `inventario`;

CREATE DATABASE `inventario` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `inventario`;

CREATE TABLE `contratos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `vencimiento` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `insumos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `cantidad` int(10) unsigned NOT NULL,
  `unidadMedida` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tipos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `sucursales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plaza` varchar(30) NOT NULL,
  `ciudad` varchar(30) NOT NULL,
  `idTipo` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `plaza` (`plaza`),
  KEY `idTipo` (`idTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idSucursal` int(10) unsigned NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `permisos` int(10) unsigned NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `idSucursal` (`idSucursal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `productos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idCategoria` int(10) unsigned NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `cantidad` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`, `idCategoria`),
  KEY `idCategoria` (`idCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `almacen` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idSucursal` int(10) unsigned NOT NULL,
  `idProducto` int(10) unsigned NOT NULL,
  `cantidadAlmacen` int(10) unsigned NOT NULL DEFAULT '0',
  `cantidadDistribucion` int(10) unsigned NOT NULL DEFAULT '0',
  `cantidadDistribuido` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idSucursal` (`idSucursal`),
  KEY `idProducto` (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `movimientos` (
  `idUsuario` int(10) unsigned NOT NULL,
  `idProducto` int(10) unsigned NOT NULL,
  `cantidad` int(10) unsigned NOT NULL,
  `tipo` tinyint(1) NOT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `idUsuario` (`idUsuario`),
  KEY `idProducto` (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `bajas` (
  `idUsuario` int(10) unsigned NOT NULL,
  `idProducto` int(10) unsigned NOT NULL,
  `cantidad` int(10) unsigned NOT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `idUsuario` (`idUsuario`),
  KEY `idProducto` (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `sucursales`
ADD CONSTRAINT `sucursales_ibfk_1` FOREIGN KEY (`idTipo`) REFERENCES `tipos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `usuarios`
ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`idSucursal`) REFERENCES `sucursales` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `productos`
ADD CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`idCategoria`) REFERENCES `categorias` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `almacen`
ADD CONSTRAINT `almacen_ibfk_1` FOREIGN KEY (`idSucursal`) REFERENCES `sucursales` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `almacen_ibfk_2` FOREIGN KEY (`idProducto`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `movimientos`
ADD CONSTRAINT `movimientos_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `movimientos_ibfk_2` FOREIGN KEY (`idProducto`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `bajas`
ADD CONSTRAINT `bajas_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `bajas_ibfk_2` FOREIGN KEY (`idProducto`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

-- Llenando base de datos

-- Llenando contratos
INSERT INTO `contratos` (`id`, `nombre`, `tipo`, `vencimiento`) VALUES
(1, 'Empresa trasportista, S.A.', 'Arrendamiento Transporte', '10/10/2020' ),
(2, 'Viajes, S.A.', 'Maquinaria de Camiones', '01/01/2025' );
-- Llenando insumos
INSERT INTO `insumos` (`id`, `nombre`, `cantidad`, `unidadMedida`) VALUES
(1, 'Combustible', 10000,  'Galones'),
(2, 'Llantas', 50, 'Unidad' ),
(3, 'Repuestos', 50, 'Unidad' ),
(4, 'Maquinaria propia', 50000, 'Horas' ),
(5, 'Maquinaria alquilada', 20000, 'Horas' );
-- Llenando tipos
INSERT INTO `tipos` (`id`, `nombre`, `descripcion`) VALUES
(1, 'Administrativa', 'Sucursal Administrativa' ),
(2, 'Planta', 'Sucursal Planta' );
-- Llenando sucursales
INSERT INTO `sucursales` (`id`, `plaza`, `ciudad`, `idTipo`) VALUES
(1, 'Cobán', 'Guatemala', 2),
(2, 'El Rancho', 'Guatemala', 2),
(3, 'Escuintla', 'Guatemala', 2),
(4, 'Guatemala', 'Guatemala', 1),
(5, 'Huehuetenango', 'Guatemala', 2),
(6, 'Petén', 'Guatemala', 2),
(7, 'Quetzaltenango', 'Guatemala', 2);
-- Llenando usuarios
INSERT INTO `usuarios` (`id`, `idSucursal`, `username`, `password`, `permisos`, `nombre`, `apellido`, `status`) VALUES
(1, 1, 'jmejicanos', '12345678', 2, 'Jorge', 'Mejicanos', 1),
(2, 1, 'admin', '12345678', 1, 'admin', 'test', 1);
-- Llenando categorias
INSERT INTO `categorias` (`id`, `nombre`, `descripcion`) VALUES
(1, 'Local', 'Distribución local' ),
(2, 'Puerto', 'Distribución internacional' );
-- Llenando productos
INSERT INTO `productos` (`id`,`idCategoria`, `nombre`, `descripcion`, `codigo`, `cantidad`) VALUES
(1, 1, "Piedrín", "Piedrín para construcción", "1" , 100 ),
(2, 1, "Cal", "Cal", "2" , 100 ),
(3, 1, "Puzolana", "Puzolana para cemento", "3" , 200 ),
(4, 1, "Arcilla", "Arcilla común", "4" , 50000 );
-- Llenando productos
INSERT INTO `almacen` (`id`,`idProducto`, `idSucursal` ) VALUES
( 1, 1, 1 ),
( 2, 2, 1 ),
( 3, 3, 1 ),
( 4, 4, 1 ),
( 5, 1, 2 ),
( 6, 2, 2 ),
( 7, 3, 2 ),
( 8, 4, 2 ),
( 9, 1, 3 ),
( 10, 2, 3 ),
( 11, 3, 3 ),
( 12, 4, 3 ),
( 13, 1, 5 ),
( 14, 2, 5 ),
( 15, 3, 5 ),
( 16, 4, 5 ),
( 17, 1, 6 ),
( 18, 2, 6 ),
( 19, 3, 6 ),
( 20, 4, 6 ),
( 21, 1, 7 ),
( 22, 2, 7 ),
( 23, 3, 7 ),
( 24, 4, 7 );

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;