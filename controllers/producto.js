/**
 * Created by Jorge Mejicanos
 */
'use strict'

const ProductModel = require('../models/producto'),
      CategoryModel = require('../models/categoria'),
      SucursalModel = require('../models/sucursal'),
      AlmacenModel = require('../models/almacen'),
      Utilidad = require('../ayuda/utilidad')

function productsGet(req, res) {
    CategoryModel.getNamesOfCategories((error, categorias) => {
        if(!error){
            res.render('./products/manager', { usuario: req.session.user, categorias })        
        }
    })
}

function productsPost(req, res){
    let categoria = req.body.categoria
    // buscas todos los productos
    ProductModel.getProductsByCategory( categoria, (error, productos) => { // si no hubo error
        if(!error) res.send(productos)
    })
}

function productsNewGet(req, res) {
    // busca el nombre de las categorias
    CategoryModel.getNamesOfCategories( (error, categorias) => { // si no hubo error
        (error) ? (
            Utilidad.printError(res, { msg: `Error al obtener las categorias: ${error}`, tipo: 0})
        ) : (
            res.render("./products/new",{ usuario: req.session.user, categorias })
        )
    })
}

function productsNewPost(req, res) {
    // variables necesarias
    let nombreCategoria = req.body.categoria
    // busco la categoria
    CategoryModel.getIdCategoryByName(nombreCategoria, (error, idCategoria) => {
        if(error){
            Utilidad.printError(res, { msg: `Error al buscar el id de la categoria: ${error}`, tipo: 0})
            return
        }
        // crea el nuevo producto
        let nuevoProducto = {
            nombre: req.body.nombre,
            descripcion: req.body.descripcion,
            codigo: req.body.codigo,
            cantidad: req.body.cantidad,
            idCategoria
        }
        // guarda el nuevo producto en la base de datos
        ProductModel.createProduct(nuevoProducto, error => { // si no hubo error al guardarlo
            if(error) {
                Utilidad.printError(res, {msg: `Error al guardar el nuevo producto: ${error}`, tipo: 1})
            } else {
                generarAlmacenes(req, res, nuevoProducto.codigo)
                // cuando termine de generar los almacenes
                //res.redirect('/products')
                res.json({msg:"",tipo:3})
            }
        })
    })
}

function productsIdProductoGet(req, res) {
    // declaro variables necesarias
    let usuario = req.session.user,
        id = req.params.id
    // busca el nombre de las categorias
    CategoryModel.getNamesOfCategories( (error, categorias) => { // si no hubo error
        if(error){
            Utilidad.printError(res, { msg: `Error al obtener las categorias: ${error}`, tipo: 0})
            return
        }
        // busco el producto a editar
        ProductModel.getProductById(id, (error, productoUpdate) => { // si no hubo error
            if(error){
                Utilidad.printError(res, { msg: `Error al obtener el producto: ${error}`, tipo: 0})
            }else{  
                (comprobarProducto(productoUpdate)) ? (
                    req.session.productoUpdate = productoUpdate,
                    res.render("./products/update",{ usuario, categorias, productoUpdate })
                ) : (
                    res.redirect('/products')
                )
                
            }
        })
    })
}

function productsIdProductoPut(req, res) {
    // variables necesarias
    let nombreCategoria = req.body.categoria,
        id = req.params.id
    // busca la categoria elegida
    CategoryModel.getIdCategoryByName(nombreCategoria, (error, idCategoria) => { // si no hubo error
        if(error){
            Utilidad.printError(res, { msg: `Error al buscar el id de la categoria: ${error}`, tipo: 0})
            return
        }
        // crea el producto ya editado
        let productoUpdate = {
            id,
            nombre: req.body.nombre,
            descripcion: req.body.descripcion,
            codigo: req.body.codigo,
            cantidad: req.body.cantidad,
            idCategoria
        }
        // guarda el nuevo producto en la base de datos
        ProductModel.updateProduct(productoUpdate, error => { // si no hubo error al guardarlo
            if(error) {
                Utilidad.printError(res, {msg: `Error al editar el producto: ${error}`, tipo: 1})
            } else {
               // if(productoUpdate.esbasico && !req.session.productoUpdate.esBasico){ 
                //    generarBasicosEnUso(req, res, productoUpdate.codigo)
                //}
                //res.redirect('/products')
                res.json({msg:"",tipo:3})
            }
        })
    })
}

function productsIdProductoDelete(req, res) {
    let id = req.params.id
    // borras el producto
    ProductModel.deleteProduct(id, error => {
        if(error) Utilidad.printError(res, {msg:`Error al borrar producto: ${error}`, tipo: 0})
        res.redirect('/products')
    })
}

function generarAlmacenes(req, res, productCode) {
    // cuando se crea un producto, ese producto se registra en el almacen de cada sucursal
    // busco el producto agregado para obtener el id
    ProductModel.getIdProductoByCode(productCode, (error, producto) => {
        if(error){ // si hubo error
            Utilidad.printError(res, { msg: `Error al obtener el id del producto: ${error}`, tipo: 0})
            return
        }
        // agregar el producto a las sucursales
        SucursalModel.getIdSucursalOfSucursales( (error, sucursales) => {
            if(error){ // si hubo error
                Utilidad.printError(res, {msg:`Error al obtener el id de las sucursales: ${error}`, tipo: 0})
                return
            }
            // genero un ciclo para generar el almacen de ese producto en cada sucursal
            sucursales.forEach(sucursal => generalAlmacen(req, res, sucursal.id, producto.id))
        })
    })
}

function generalAlmacen(req, res, idSucursal, idProducto) {
    // genera el almacen para la sucursal y el producto
    let nuevoAlmacen = {
        idProducto,
        idSucursal
    }
    AlmacenModel.createAlmacen(nuevoAlmacen, error => {
        if(error) Utilidad.printError(res, {msg:`Error al crear el almacen: ${error}`, tipo: 1})
    })
}

function comprobarProducto(producto){
    try {
        return producto.id != null
    } catch (error) {
        return false
    }
}

module.exports = {
    productsGet,
    productsPost,
    productsNewGet,
    productsNewPost,
    productsIdProductoGet,
    productsIdProductoPut,
    productsIdProductoDelete
}
