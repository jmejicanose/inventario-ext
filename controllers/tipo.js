/**
 * Created by Jorge Mejicanos
 */
'use strict'

const   TipoModel = require('../models/tipo'),
        ProductModel = require('../models/producto'),
        AlmacenModel = require('../models/almacen'),
        Utilidad = require('../ayuda/utilidad')

function tiposGet(req, res) {
    // busco las tipos
    TipoModel.getTipos( (error, tipos) => {
        (error) ? ( // si hubo error
            Utilidad.printError(res, { msg: `Error al obtener las tipos: ${error}`, tipo: 0})
        ) : ( // si no hubo error
            res.render('./tipos/manager', { tipos, usuario: req.session.user })
        )
    })
}

function tiposNewGet(req, res) {
    res.render('./tipos/new', { usuario : req.session.user })
}

function tiposNewPost(req, res) {
    // creo el nueva tipo
    let nuevaTipo = {
        nombre: req.body.nombre,
        descripcion: req.body.descripcion
    }
    // guardo el nueva tipo en la base de datos
    TipoModel.createTipo(nuevaTipo, error => {
        if(error){ // si hubo error
            Utilidad.printError(res, { msg: `Error al guardar el nueva tipo, nombre repetida: ${error}`, tipo: 1})
        } else { // si no hubo error
            //res.redirect('/tipos')
            res.json({msg:"",tipo:3})
            // genero los almacenes para el tipo
            // con los productos existentes
            // busco el id de el tipo que se acaba de crear
            TipoModel.getIdTipoByNombre(nuevaTipo.nombre, (error, id) => {
                (error) ? (
                    Utilidad.printError(res, {msg: `Error al buscar el id de el tipo: ${error}`, tipo: 0})
                ) : (
                    generarAlmacenes(req, res, id)
                )
            })
        }
    })
}

function tiposIdTipoGet(req, res) {
    // declaro variables necesarias
    let id = req.params.id,
        usuario = req.session.user
    // busco el tipo a editar
    TipoModel.getTipoById(id, (error, tipoUpdate) => {
        (error) ? ( // si hubo error
            Utilidad.printError(res, { msg: `Error al obtener el tipo: ${error}`, tipo: 0 })
        ) : ( // si no hubo error
            (comprobarTipo(tipoUpdate)) ? (
                res.render('./tipos/update', {  usuario, tipoUpdate })
            ) : (
                res.redirect('/tipos')
            )

        )
    })
}

function tiposIdTipoPut(req, res) {
    // obtengo el tipo con los cambios realizados
    let tipoUpdate = {
        id: req.params.id,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion
    }
    // actualizo el tipo en la base de datos
    TipoModel.updateTipo(tipoUpdate, error => {
        (error) ? ( // si hubo error
            Utilidad.printError(res, { msg: `Error al actualizar tipo, nombre repetida: ${error}`, tipo: 1 })
        ) : ( // si no hubo error
            //res.redirect('/tipos')
            res.json({msg:"",tipo:3})
        )
    })
}
// pendiente, no se como eliminar por cascada, aun
function tiposIdTipoDelete(req, res) {
    let id = req.params.id
    // elimina el tipo, y realiza eliminacion por cascada
    TipoModel.deleteTipo(id, error => {
        if(error) Utilidad.printError(res, {msg:`Error al eliminar el tipo: ${error}`, tipo: 0})
        res.redirect('/tipos')
    })
}

function generarAlmacenes(req, res, id){
    // busco todos los productos registrados
    ProductModel.getAllIdProducto((error, productos) => {
        if(error){
            Utilidad.printError(res, {msg: `Error al obtener los ids de los productos: ${error}`, tipo: 0})
        }else{
            let values = []
            for(let i=0 ; productos[i] ; i++){
                values.push([ id, productos[i].idProducto])
            }     
            
            AlmacenModel.createAlmacenes(values, (error) => {
                if(error) Utilidad.printError(res, {msg:`Error al crear el almacen: ${error}`, tipo: 1})
            })
        }
    })
}

function comprobarTipo(tipoUpdate){
    try {
        return tipoUpdate.id != null
    } catch (error) {
        return false
    }
}

module.exports = {
    tiposGet,
    tiposNewGet,
    tiposNewPost,
    tiposIdTipoGet,
    tiposIdTipoPut,
    tiposIdTipoDelete
}