/**
 * Created by Jorge Mejicanos
 */
'use strict'

const AlmacenModel = require('../models/almacen'),
    BajaModel = require('../models/baja'),
    CategoryModel = require('../models/categoria'),
    SucursalModel = require('../models/sucursal'),
    Utilidad = require('../ayuda/utilidad')

function distribuidoGet(req, res) {
    let usuario = req.session.user

    // muestra la vista de los productos en distribuido
    SucursalModel.getSucursalesPlanta((error, sucursales) => {
        if (!error) {
            CategoryModel.getNamesOfCategories((error, categorias) => {
                if (!error) {
                    res.render('./distribuido/manager', { usuario, sucursales, categorias })
                }
            })
        }
    })
}

function distribuidoPost(req, res) {
    let categoria = req.body.categoria, // obtienes el nombre de la categoria
        sucursal = req.body.plaza;

    // obtengo el distribuido
    AlmacenModel.getConsumoByPlazaAndCategory(sucursal, categoria, (error, distribuido) => {
        if (!error) res.send(distribuido) // se envia el distribuido con los productos de la caterogia seleccionada            
    })
}

function distribuidoIdDistribuidoPut(req, res) {
    // obtenemos la cantidad
    let cantidad = parseInt(req.body.cantidad)
    // si no mandaron cambios
    if (isNaN(cantidad) || cantidad === 0) {
        res.send("")
    } else { // quitamos lo productos de distribuido
        let usuario = req.session.user,
            idAlmacen = req.params.id

        // obtengo el almacen
        AlmacenModel.getConsumoById(idAlmacen, (error, almacen) => {
            if (error) { // si hubo error
                Utilidad.printError(res, { msg: `Error al obtener el almacen: ${error}`, tipo: 0 })
            } else if (almacen.cantidadDistribuido === 0) { // si no hay nada en distribuido
                res.send("")
            } else { // si no hubo error
                // genero los cambios
                let verificar = (cantidad >= almacen.cantidadDistribuido),
                    almacenUpdate = {
                        id: idAlmacen,
                        cantidadDistribuido: (verificar) ? (0) : (almacen.cantidadDistribuido - cantidad)
                    }
                // guardo los cambios
                AlmacenModel.updateAlmacen(almacenUpdate, error => {
                    if (error) {
                        Utilidad.printError(res, { msg: `Error al actualizar el almacen: ${error}`, tipo: 0 })
                    } else {
                        // creo el movimiento
                        let baja = {
                            idUsuario: usuario.id,
                            idProducto: almacen.idProducto,
                            cantidad: (verificar) ? (almacen.cantidadDistribuido) : (cantidad),
                            fecha: fechaActual()
                        }
                        // guardo el movimiento que ocurrio
                        BajaModel.createBaja(baja, error => {
                            (error) ? ( // si hubo error
                                Utilidad.printError(res, { msg: `Error al crear el movimiento: ${error}`, tipo: 0 })
                            ) : ( // si no hubo
                                    // mando la nueva cantidad del almacen
                                    (verificar) ? (res.send('0')) : (res.send(`${almacenUpdate.cantidadDistribuido}`))
                                )
                        })
                    }
                })
            }
        })
    }
}

function fechaActual() {
    let fecha = new Date(); // obtengo la fecha actual
    fecha.setHours(fecha.getHours() - 7) // le resto 7 horas a la hora actual
    return fecha // regreso la fecha
}

module.exports = {
    distribuidoGet,
    distribuidoIdDistribuidoPut,
    distribuidoPost
}