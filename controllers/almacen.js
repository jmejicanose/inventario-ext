/**
 * Created by Jorge Mejicanos
 */
'use strict'

const AlmacenModel = require('../models/almacen'),
    MovimientoModel = require('../models/movimiento'),
    CategoryModel = require('../models/categoria'),
    SucursalModel = require('../models/sucursal'),
    Utilidad = require('../ayuda/utilidad')

function almacenGet(req, res) {
    let usuario = req.session.user

    // muestra la vista del almacen
    SucursalModel.getSucursalesPlanta((error, sucursales) => {
        if (!error) {
            CategoryModel.getNamesOfCategories((error, categorias) => {
                if (!error) {
                    res.render('./almacenados/manager', { usuario, sucursales, categorias })
                }
            })
        }
    })
}

function almacenPost(req, res) {
    let categoria = req.body.categoria, // obtienes el nombre de la categoria
        sucursal = req.body.plaza;

        // obtengo el almacen
        AlmacenModel.getAlmacenByPlazaAndCategory(sucursal, categoria, (error, almacen) => {
            if (!error) res.send(almacen) // se envia el almacen con los productos de la caterogia seleccionada
        })
}

function almacenIdAlmacenAddPut(req, res) {
    // obtengo la cantidad
    let cantidad = parseInt(req.body.cantidad)
    // si no mandaron cambios
    if (isNaN(cantidad) || cantidad === 0) {
        res.send("") // no mando nada
    } else { // si agregaron productos
        let usuario = req.session.user,
            id = req.params.id

        // busco el almacen
        AlmacenModel.getAlmacenById(id, (error, almacen) => {
            if (error) { // si ocurrio un error
                Utilidad.printError(res, { msg: `Error al obtener el almacen: ${error}`, tipo: 0 })
            } else { // si no paso nada malo
                // actualizo el almacen
                let almacenUpdate = {
                    id,
                    cantidadAlmacen: almacen.cantidadAlmacen + cantidad
                }
                // guardo los cambios
                AlmacenModel.updateAlmacen(almacenUpdate, error => {
                    if (error) { // si hubo error
                        Utilidad.printError(res, { msg: `Error al actualizar el almacen: ${error}`, tipo: 0 })
                    } else { // si no hubo error
                        // creo el movimiento
                        let movimiento = {
                            idUsuario: usuario.id,
                            idProducto: almacen.idProducto,
                            cantidad,
                            tipo: 1, // es una alta
                            fecha: fechaActual()
                        }
                        // guardo el movimiento que ocurrio
                        MovimientoModel.createMovimientoNoBasico(movimiento, error => {
                            (error) ? ( // si hubo error
                                Utilidad.printError(res, { msg: `Error al crear el movimiento: ${error}`, tipo: 0 })
                            ) : ( // si no hubo
                                    // mando la nueva cantidad del almacen
                                    res.send('' + almacenUpdate.cantidadAlmacen)
                                )
                        })
                    }
                })
            }
        })
    }
}

function almacenIdAlmacenSubPut(req, res) {
    // obtengo la cantidad
    let cantidad = parseInt(req.body.cantidad)
    // si no mandaron cambios
    if (isNaN(cantidad) || cantidad === 0) {
        res.send("") // no mando nada
    } else { // si se quitaron productos
        let usuario = req.session.user,
            id = req.params.id

        // obtengo el almacen
        AlmacenModel.getConsumoById(id, (error, almacen) => {
            if (error) { // si hubo error
                Utilidad.printError(res, { msg: `Error al obtener el almacen: ${error}`, tipo: 0 })
            } else if (almacen.cantidadAlmacen === 0) { // si no hay productos en el almacen
                res.send("")
            } else { // si no hubo error
                // genero los cambios
                let verificar = (cantidad >= almacen.cantidadAlmacen),
                    almacenUpdate = {
                        id,
                        cantidadAlmacen: (verificar) ? (0) : (almacen.cantidadAlmacen - cantidad),
                        cantidadDistribucion: (verificar) ? (almacen.cantidadDistribucion + almacen.cantidadAlmacen) : (almacen.cantidadDistribucion + cantidad)
                    }
                // guardo los cambios
                AlmacenModel.updateAlmacen(almacenUpdate, error => {
                    if (error) {
                        Utilidad.printError(res, { msg: `Error al actualizar el almacen: ${error}`, tipo: 0 })
                    } else {
                        // creo el movimiento
                        let movimiento = {
                            idUsuario: usuario.id,
                            idProducto: almacen.idProducto,
                            cantidad: (verificar) ? (almacen.cantidadAlmacen) : (cantidad),
                            tipo: 0, // es una baja
                            fecha: fechaActual()
                        }
                        // guardo el movimiento que ocurrio
                        MovimientoModel.createMovimientoNoBasico(movimiento, error => {
                            (error) ? ( // si hubo error
                                Utilidad.printError(res, { msg: `Error al crear el movimiento: ${error}`, tipo: 0 })
                            ) : ( // si no hubo
                                    // mando la nueva cantidad del almacen
                                    (verificar) ? (res.send('0')) : (res.send(`${almacenUpdate.cantidadAlmacen}`))
                                )
                        })
                    }
                })
            }
        })
    }
}

function fechaActual() {
    let fecha = new Date(); // obtengo la fecha actual
    fecha.setHours(fecha.getHours() - 7) // le resto 7 horas a la hora actual
    return fecha // regreso la fecha
}

module.exports = {
    almacenGet,
    almacenIdAlmacenAddPut,
    almacenIdAlmacenSubPut,
    almacenPost
}