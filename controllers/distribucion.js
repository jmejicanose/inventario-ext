/**
 * Created by Jorge Mejicanos
 */
'use strict'

const AlmacenModel = require('../models/almacen'),
    MovimientoModel = require('../models/movimiento'),
    CategoryModel = require('../models/categoria'),
    SucursalModel = require('../models/sucursal'),
    Utilidad = require('../ayuda/utilidad')

function distribucionGet(req, res) {
    let usuario = req.session.user

    // muestra la vista de los productos en distribucion
    SucursalModel.getSucursalesPlanta((error, sucursales) => {
        if (!error) {
            CategoryModel.getNamesOfCategories((error, categorias) => {
                if (!error) {
                    res.render('./distribucion/manager', { usuario, sucursales, categorias })
                }
            })
        }
    })
}

function distribucionPost(req, res) {
    let categoria = req.body.categoria, // obtienes el nombre de la categoria
        sucursal = req.body.plaza;
        
    // obtengo el distribucion
    AlmacenModel.getConsumoByPlazaAndCategory(sucursal, categoria, (error, distribucion) => {
        if (!error) res.send(distribucion) // se envia el distribucion con los productos de la caterogia seleccionada            
    })
}

function distribucionIdDistribucionPut(req, res) {
    // obtenemos la cantidad
    let cantidad = parseInt(req.body.cantidad)
    // si no mandaron cambios
    if (isNaN(cantidad) || cantidad === 0) {
        res.send("")
    } else { // quitamos lo productos de distribucion
        let usuario = req.session.user,
            idAlmacen = req.params.id

        // obtengo el almacen
        AlmacenModel.getConsumoById(idAlmacen, (error, almacen) => {
            if (error) { // si hubo error
                Utilidad.printError(res, { msg: `Error al obtener el almacen: ${error}`, tipo: 0 })
            } else if (almacen.cantidadDistribucion === 0) { // si no hay nada en distribucion
                res.send("")
            } else { // si no hubo error
                // genero los cambios
                let verificar = (cantidad >= almacen.cantidadDistribucion),
                    almacenUpdate = {
                        id: idAlmacen,
                        cantidadDistribucion: (verificar) ? (0) : (almacen.cantidadDistribucion - cantidad),
                        cantidadDistribuido: (verificar) ? (almacen.cantidadDistribuido + almacen.cantidadDistribucion) : (almacen.cantidadDistribuido + cantidad)
                    }
                // guardo los cambios
                AlmacenModel.updateAlmacen(almacenUpdate, error => {
                    if (error) {
                        Utilidad.printError(res, { msg: `Error al actualizar el almacen: ${error}`, tipo: 0 })
                    } else {
                        // creo el movimiento
                        let movimiento = {
                            idUsuario: usuario.id,
                            idProducto: almacen.idProducto,
                            cantidad: (verificar) ? (almacen.cantidadDistribucion) : (cantidad),
                            tipo: 2, // es una distribucion
                            fecha: fechaActual()
                        }
                        // guardo el movimiento que ocurrio
                        MovimientoModel.createMovimientoNoBasico(movimiento, error => {
                            (error) ? ( // si hubo error
                                Utilidad.printError(res, { msg: `Error al crear el movimiento: ${error}`, tipo: 0 })
                            ) : ( // si no hubo
                                    // mando la nueva cantidad del almacen
                                    (verificar) ? (res.send('0')) : (res.send(`${almacenUpdate.cantidadDistribucion}`))
                                )
                        })
                    }
                })
            }
        })
    }
}

function fechaActual() {
    let fecha = new Date(); // obtengo la fecha actual
    fecha.setHours(fecha.getHours() - 7) // le resto 7 horas a la hora actual
    return fecha // regreso la fecha
}

module.exports = {
    distribucionGet,
    distribucionIdDistribucionPut,
    distribucionPost
}