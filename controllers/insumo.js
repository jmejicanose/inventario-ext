/**
 * Created by Jorge Mejicanos
 */
'use strict'

const InsumoModel = require('../models/insumo'),
    Utilidad = require('../ayuda/utilidad')

function insumosGet(req, res) {
    // busco los insumos
    InsumoModel.getInsumos((error, insumos) => {
        (error) ? ( // si hubo error
            Utilidad.printError(res, { msg: `Error al obtener los insumos: ${error}`, insumo: 0 })
        ) : ( // si no hubo error
                res.render('./insumos/manager', { insumos, usuario: req.session.user })
            )
    })
}

function insumosNewGet(req, res) {
    res.render('./insumos/new', { usuario: req.session.user })
}

function insumosNewPost(req, res) {
    // creo el nuevo insumo
    let nuevoInsumo = {
        nombre: req.body.nombre,
        cantidad: req.body.cantidad,
        unidadMedida: req.body.unidad
    }
    // guardo el nuevo insumo en la base de datos
    InsumoModel.createInsumo(nuevoInsumo, error => {
        if (error) { // si hubo error
            Utilidad.printError(res, { msg: `Error al guardar el nuevo insumo, nombre repetido: ${error}`, insumo: 1 })
        } else { // si no hubo error
            //res.redirect('/insumos')
            res.json({ msg: "", insumo: 3 })
        }
    })
}

function insumosIdInsumoGet(req, res) {
    // declaro variables necesarias
    let id = req.params.id,
        usuario = req.session.user
    // busco el insumo a editar
    InsumoModel.getInsumoById(id, (error, insumoUpdate) => {
        (error) ? ( // si hubo error
            Utilidad.printError(res, { msg: `Error al obtener el insumo: ${error}`, insumo: 0 })
        ) : ( // si no hubo error
                (comprobarInsumo(insumoUpdate)) ? (
                    res.render('./insumos/update', { usuario, insumoUpdate })
                ) : (
                        res.redirect('/insumos')
                    )

            )
    })
}

function insumosIdInsumoPut(req, res) {
    // obtengo el insumo con los cambios realizados
    let insumoUpdate = {
        id: req.params.id,
        nombre: req.body.nombre,
        cantidad: req.body.cantidad,
        unidadMedida: req.body.unidad
    }
    // actualizo el insumo en la base de datos
    InsumoModel.updateInsumo(insumoUpdate, error => {
        (error) ? ( // si hubo error
            Utilidad.printError(res, { msg: `Error al actualizar insumo, nombre repetida: ${error}`, insumo: 1 })
        ) : ( // si no hubo error
                //res.redirect('/insumos')
                res.json({ msg: "", insumo: 3 })
            )
    })
}
// pendiente, no se como eliminar por cascada, aun
function insumosIdInsumoDelete(req, res) {
    let id = req.params.id
    // elimina el insumo, y realiza eliminacion por cascada
    InsumoModel.deleteInsumo(id, error => {
        if (error) Utilidad.printError(res, { msg: `Error al eliminar el insumo: ${error}`, insumo: 0 })
        res.redirect('/insumos')
    })
}

function comprobarInsumo(insumoUpdate) {
    try {
        return insumoUpdate.id != null
    } catch (error) {
        return false
    }
}

module.exports = {
    insumosGet,
    insumosNewGet,
    insumosNewPost,
    insumosIdInsumoGet,
    insumosIdInsumoPut,
    insumosIdInsumoDelete
}