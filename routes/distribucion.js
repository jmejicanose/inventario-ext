/**
 * Created by Jorge Mejicanos
 */
'use strict'

const express = require("express"),
    DistribucionController = require('../controllers/distribucion'),
    distribucion = express.Router()

// inventario/distribucion
distribucion.get("/", DistribucionController.distribucionGet)
// inventario/categoria/categoria/get
distribucion.post('/', DistribucionController.distribucionPost)
// inventario/distribucion/:id
distribucion.put("/:id", DistribucionController.distribucionIdDistribucionPut)

module.exports = distribucion