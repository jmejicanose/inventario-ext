/**
 * Created by Jorge Mejicanos
 */
'use strict'

const InsumoModel = require('./coneccion')

function getInsumoById(id, next) {
    InsumoModel
        .query(`SELECT * 
                FROM insumos i 
                WHERE i.id = ?`, id, (error, resultado, fields) => {

                next(error, resultado[0])
            })
}

function getIdInsumoByNombre(nombre, next) {
    InsumoModel
        .query(`SELECT s.id 
                FROM insumos s 
                WHERE s.nombre = ?`, nombre, (error, resultado, fields) => {
                try { // si suerge un error es de la base de datos
                    next(error, resultado[0].id)
                } catch (error) { // si no se encontro sucursal
                    next(error = null, 0)
                }
            })
}

function getNamesOfInsumos(next) {
    InsumoModel
        .query(`SELECT t.nombre 
                FROM insumos t`, (error, resultado, fields) => {

                next(error, resultado)
            })
}

function getInsumos(next) {
    InsumoModel
        .query(`SELECT * 
                FROM insumos`, (error, resultado, fields) => {

                next(error, resultado)
            })
}

function createInsumo(insumo, next) {
    InsumoModel
        .query(`INSERT INTO insumos 
                SET ?`, insumo, (error, resultado, fields) => {

                next(error)
            })
}

function updateInsumo(insumo, next) {
    InsumoModel
        .query(`UPDATE insumos s 
                SET ? 
                WHERE s.id = ?`, [insumo, insumo.id], (error, resultado, fields) => {

                next(error)
            })
}

function deleteInsumo(id, next) {
    InsumoModel
        .query(`DELETE FROM insumos 
                WHERE id = ? `, id, (error, resultado, fields) => {

                next(error)
            })
}

module.exports = {
    getInsumoById,
    getIdInsumoByNombre,
    getNamesOfInsumos,
    getInsumos,
    createInsumo,
    updateInsumo,
    deleteInsumo
}