/**
 * Created by Jorge Mejicanos
 */
'use strict'

const MovimientoModel = require('./coneccion')

function getMovimientosNoBasicosByIdSucursalAndCategoria(idSucursal, inicio, final, next) {
    MovimientoModel
        .query(`SELECT p.codigo, p.nombre nombreProducto, m.cantidad, m.tipo, concat(u.nombre,' ',u.apellido) nombreUsuario, m.fecha
                FROM movimientos m
                JOIN productos p ON m.idProducto = p.id
                JOIN usuarios u ON m.idUsuario = u.id
                WHERE (m.fecha BETWEEN ? AND ?) AND u.idSucursal = ? `, [inicio, final, idSucursal], (error, resultado, fields) => {

            next(error, resultado)
        })
}

function getMovimientosNoBasicosByPlazaAndCategoria(plaza, inicio, final, next) {
    MovimientoModel
        .query(`SELECT p.codigo, p.nombre nombreProducto, m.cantidad, m.tipo, concat(u.nombre,' ',u.apellido) nombreUsuario, m.fecha
                FROM movimientos m
                JOIN productos p ON m.idProducto = p.id
                JOIN usuarios u ON m.idUsuario = u.id
                JOIN sucursales s ON u.idSucursal = s.id
                WHERE (m.fecha BETWEEN ? AND ?) AND s.plaza = ? `, [inicio, final, plaza], (error, resultado, fields) => {

            next(error, resultado)
        })
}

function createMovimientoNoBasico(movimiento, next) {
    MovimientoModel
        .query(`INSERT INTO movimientos 
                SET ?`, movimiento, (error, resultado, fields) => {

            next(error)
        })
}

module.exports = {
    getMovimientosNoBasicosByIdSucursalAndCategoria,
    getMovimientosNoBasicosByPlazaAndCategoria,
    createMovimientoNoBasico
}