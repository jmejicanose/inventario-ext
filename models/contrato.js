/**
 * Created by Jorge Mejicanos
 */
'use strict'

const ContratoModel = require('./coneccion')

function getContratoById(id, next) {
    ContratoModel
        .query(`SELECT * 
                FROM contratos i 
                WHERE i.id = ?`, id, (error, resultado, fields) => {

                next(error, resultado[0])
            })
}

function getIdContratoByNombre(nombre, next) {
    ContratoModel
        .query(`SELECT s.id 
                FROM contratos s 
                WHERE s.nombre = ?`, nombre, (error, resultado, fields) => {
                try { // si suerge un error es de la base de datos
                    next(error, resultado[0].id)
                } catch (error) { // si no se encontro sucursal
                    next(error = null, 0)
                }
            })
}

function getNamesOfContratos(next) {
    ContratoModel
        .query(`SELECT t.nombre 
                FROM contratos t`, (error, resultado, fields) => {

                next(error, resultado)
            })
}

function getContratos(next) {
    ContratoModel
        .query(`SELECT * 
                FROM contratos`, (error, resultado, fields) => {

                next(error, resultado)
            })
}

function createContrato(contrato, next) {
    ContratoModel
        .query(`INSERT INTO contratos 
                SET ?`, contrato, (error, resultado, fields) => {

                next(error)
            })
}

function updateContrato(contrato, next) {
    ContratoModel
        .query(`UPDATE contratos s 
                SET ? 
                WHERE s.id = ?`, [contrato, contrato.id], (error, resultado, fields) => {

                next(error)
            })
}

function deleteContrato(id, next) {
    ContratoModel
        .query(`DELETE FROM contratos 
                WHERE id = ? `, id, (error, resultado, fields) => {

                next(error)
            })
}

module.exports = {
    getContratoById,
    getIdContratoByNombre,
    getNamesOfContratos,
    getContratos,
    createContrato,
    updateContrato,
    deleteContrato
}