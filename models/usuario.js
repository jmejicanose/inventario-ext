/**
 * Created by Jorge Mejicanos
 */
'use strict'

const UserModel = require('./coneccion')


function getUserById(id, next) {
    UserModel
        .query(`SELECT u.id, u.username, u.nombre, u.apellido, u.status, u.permisos, u.idSucursal, s.plaza
                FROM usuarios u 
                JOIN sucursales s ON u.idSucursal = s.id
                WHERE u.id = ?`, id, (error, resultado, fields) => {

                next(error, resultado[0])
            })
}

function getUserByUsername(username, next) {
    UserModel
        .query(`SELECT u.id, u.username, u.password, u.status, u.permisos, u.idSucursal 
                FROM usuarios u 
                WHERE u.username = ? `, username, (error, resultado, fields) => {

                next(error, resultado[0])
            })
}

function getUsers(next) {
    UserModel
        .query(`SELECT u.username, u.nombre, u.apellido, u.permisos, u.status, u.id, s.plaza 
                FROM usuarios u 
                INNER JOIN sucursales s ON u.idSucursal = s.id` , (error, resultado, fields) => {

                next(error, resultado)
            })
}

function getUsersBySucursal(id, idSucursal, next) {
    UserModel
        .query(`SELECT u.username, u.nombre, u.apellido, u.permisos, u.status, u.id 
                FROM usuarios u 
                WHERE (u.idSucursal = ? AND u.permisos = 0) OR u.id = ?`, [idSucursal, id], (error, resultado, fields) => {

                next(error, resultado)
            })
}

function createUser(user, next) {
    UserModel
        .query(`INSERT INTO usuarios 
                SET ?`, user, (error, resultado, fields) => {

                next(error)
            })
}

function updateUser(user, next) {
    UserModel
        .query(`UPDATE usuarios 
                SET ? 
                WHERE id = ?`, [user, user.id], (error, resultado, fields) => {

                next(error)
            })
}


module.exports = {
    getUserById,
    getUserByUsername,
    getUsers,
    getUsersBySucursal,
    createUser,
    updateUser
}