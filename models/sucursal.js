/**
 * Created by Jorge Mejicanos
 */
'use strict'

const SucursalModel = require('./coneccion')

function getSucursalById(id, next) {
    SucursalModel
        .query(`SELECT s.id, s.plaza, s.ciudad, t.nombre nombreTipo
                FROM sucursales s 
                join tipos t on t.id = s.idTipo 
                WHERE s.id = ?`, id, (error, resultado, fields) => {

                next(error, resultado[0])
            })
}

function getIdSucursalByPlaza(plaza, next) {
    SucursalModel
        .query(`SELECT s.id 
                FROM sucursales s 
                WHERE s.plaza = ?`, plaza, (error, resultado, fields) => {
                try { // si suerge un error es de la base de datos
                    next(error, resultado[0].id)
                } catch (error) { // si no se encontro sucursal
                    next(error = null, 0)
                }
            })
}

function getPlazasOfSucursales(next) {
    SucursalModel
        .query(`SELECT s.id, s.plaza 
                FROM sucursales s`, (error, resultado, fields) => {

                next(error, resultado)
            })
}

function getSucursalesPlanta(next) {
    SucursalModel
        .query(`SELECT s.id, s.plaza 
                FROM sucursales s
                WHERE s.idTipo = 2`, (error, resultado, fields) => {

                next(error, resultado)
            })
}

function getIdSucursalOfSucursales(next) {
    SucursalModel
        .query(`SELECT s.id 
                FROM sucursales s`, (error, resultado, fields) => {

                next(error, resultado)
            })
}

function getSucursales(next) {
    SucursalModel
        .query(`SELECT s.id, s.plaza, s.ciudad, t.nombre nombreTipo
                FROM sucursales s 
                join tipos t on t.id = s.idTipo`, (error, resultado, fields) => {

                next(error, resultado)
            })
}

function createSucursal(sucursal, next) {
    SucursalModel
        .query(`INSERT INTO sucursales 
                SET ?`, sucursal, (error, resultado, fields) => {

                next(error)
            })
}

function updateSucursal(sucursal, next) {
    SucursalModel
        .query(`UPDATE sucursales s 
                SET ? 
                WHERE s.id = ?`, [sucursal, sucursal.id], (error, resultado, fields) => {

                next(error)
            })
}

function deleteSucursal(id, next) {
    SucursalModel
        .query(`DELETE FROM sucursales 
                WHERE id = ? `, id, (error, resultado, fields) => {

                next(error)
            })
}

module.exports = {
    getSucursalById,
    getSucursalesPlanta,
    getPlazasOfSucursales,
    getIdSucursalOfSucursales,
    getIdSucursalByPlaza,
    getSucursales,
    createSucursal,
    updateSucursal,
    deleteSucursal
}