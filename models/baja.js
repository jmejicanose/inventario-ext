/**
 * Created by Raul Perez on 30/04/2017.
 */
'use strict'

const BajaModel = require('./coneccion')


function createBaja(baja, next) {
    BajaModel
        .query(`INSERT INTO bajas 
                SET ?`, baja, (error, resultado, fields) => {

            next(error)
        })
}

module.exports = {
    createBaja
}