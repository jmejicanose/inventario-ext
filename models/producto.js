/**
 * Created by Jorge Mejicanos
 */
'use strict'

const ProductModel = require('./coneccion')

function getProductById(id, next) {
    ProductModel
        .query(`SELECT p.id, p.nombre, p.codigo, p.descripcion, p.cantidad, c.nombre nombreCategoria
                FROM productos p
                JOIN categorias c ON p.idCategoria = c.id
                WHERE p.id = ?`, id, (error, resultado, fields) => {

                next(error, resultado[0])
            })
}

function getIdProductoByCode(code, next) {
    ProductModel
        .query(`SELECT p.id
                FROM productos p
                WHERE p.codigo = ?`, code, (error, resultado, fields) => {

                next(error, resultado[0])
            })
}

function getAllIdProducto(next) {
    ProductModel
        .query(`SELECT p.id
                FROM productos p`, (error, resultado, fields) => {

                next(error, resultado)
            })
}

function getProductsByCategory(categoria, next) {
    ProductModel
        .query(`SELECT p.id, p.nombre, p.descripcion, p.codigo, p.cantidad
                FROM productos p
                JOIN categorias c ON p.idCategoria = c.id AND c.nombre = ?` , categoria, (error, resultado, fields) => {

                next(error, resultado)
            })
}

function createProduct(product, next) {
    ProductModel
        .query(`INSERT INTO productos
                SET ?`, product, (error, resultado, fields) => {

                next(error)
            })
}

function updateProduct(product, next) {
    ProductModel
        .query(`UPDATE productos p
                SET ?
                WHERE p.id = ?`, [product, product.id], (error, resultado, fields) => {

                next(error)
            })
}

function deleteProduct(id, next) {
    ProductModel
        .query(`DELETE FROM productos
                WHERE id = ?`, id, (error, resultado, fields) => {

                next(error)
            })
}

module.exports = {
    getProductById,
    getIdProductoByCode,
    getAllIdProducto,
    getProductsByCategory,
    createProduct,
    updateProduct,
    deleteProduct
}
