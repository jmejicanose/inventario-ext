/**
 * Created by Jorge Mejicanos
 */
'use strict'

const AlmacenModel = require('./coneccion')

function getAlmacenById(id, next) {
    AlmacenModel
        .query(`SELECT a.idProducto, a.cantidadAlmacen 
                FROM almacen a 
                WHERE id = ?` , id ,(error, resultado, fields) => {

            next(error, resultado[0])
    })
}

function getAlmacen(next) {
    AlmacenModel
        .query(`SELECT a.id, p.nombre nombreProducto, p.codigo, a.cantidadDistribucion, a.cantidadDistribuido
                FROM almacen a
                JOIN productos p ON a.idProducto = p.id` ,(error, resultado, fields) => {

            next(error, resultado)
        })
}

function getAlmacenBySucursalAndProduct(idSucursal, idProducto, next) {
    AlmacenModel
        .query(`SELECT a.id, a.cantidadAlmacen, a.cantidadDistribucion, a.cantidadDistribuido
                FROM almacen a 
                WHERE a.idSucursal = ? AND a.idProducto = ?` , [idSucursal, idProducto] ,(error, resultado, fields) => {

            next(error, resultado[0])
        })
}

function getConsumoById(id, next) {
    AlmacenModel
        .query(`SELECT a.idProducto, a.cantidadAlmacen, a.cantidadDistribucion, a.cantidadDistribuido 
                FROM almacen a 
                WHERE id = ?` , id ,(error, resultado, fields) => {

            next(error, resultado[0])
        })
}

function getAlmacenByPlazaAndCategory(plaza, categoria, next) {
    AlmacenModel
        .query(`SELECT a.id, a.cantidadAlmacen, p.nombre nombreProducto, p.codigo, p.cantidad
                FROM almacen a 
                JOIN productos p on a.idProducto = p.id
                JOIN categorias c on p.idCategoria = c.id AND c.nombre = ?
                JOIN sucursales s on a.idSucursal = s.id AND s.plaza = ?` , [categoria, plaza], (error, resultado, fields) => {

            next(error, resultado)
        })
}

function getAlmacenBySucursalAndCategory(idSucursal, categoria, next) {
    AlmacenModel
        .query(`SELECT a.id, p.nombre nombreProducto, p.codigo, a.cantidadAlmacen, p.cantidad
                FROM almacen a
                JOIN productos p ON a.idProducto = p.id
                JOIN categorias c ON p.idCategoria = c.id AND c.nombre = ?
                WHERE a.idSucursal = ?`  , [categoria, idSucursal] ,(error, resultado, fields) => {

            next(error, resultado)
        })
}

function getAlmacenByCategory(categoria, next) {
    AlmacenModel
        .query(`SELECT a.id, p.nombre nombreProducto, p.codigo, a.cantidadAlmacen, p.cantidad
                FROM almacen a
                JOIN productos p ON a.idProducto = p.id
                JOIN categorias c ON p.idCategoria = c.id 
                AND c.nombre = ?`  , [categoria] ,(error, resultado, fields) => {

            next(error, resultado)
        })
}

function getConsumoByPlazaAndCategory(plaza, categoria, next) {
    AlmacenModel
        .query(`SELECT a.id, p.nombre nombreProducto, p.codigo, a.cantidadDistribucion, a.cantidadDistribuido 
                FROM almacen a 
                JOIN productos p on a.idProducto = p.id
                JOIN categorias c on p.idCategoria = c.id AND c.nombre = ?
                JOIN sucursales s on a.idSucursal = s.id AND s.plaza = ?` 
                , [categoria, plaza], (error, resultado, fields) => {

            next(error, resultado)
        })
}

function getConsumoBySucursalAndCategory(idSucursal, categoria, next) {
    AlmacenModel
        .query(`SELECT a.id, p.nombre nombreProducto, p.codigo, c.nombre nombreCategoria, a.cantidadDistribucion, a.cantidadDistribuido
                FROM almacen a
                JOIN productos p ON a.idProducto = p.id
                JOIN categorias c ON p.idCategoria = c.id AND c.nombre = ?
                WHERE a.idSucursal = ?` , [categoria, idSucursal] ,(error, resultado, fields) => {

            next(error, resultado)
        })
}

function getConsumoByCategory(categoria, next) {
    AlmacenModel
        .query(`SELECT a.id, p.nombre nombreProducto, p.codigo, c.nombre nombreCategoria, a.cantidadDistribucion, a.cantidadDistribuido
                FROM almacen a
                JOIN productos p ON a.idProducto = p.id
                JOIN categorias c ON p.idCategoria = c.id 
                AND c.nombre = ?` , [categoria] ,(error, resultado, fields) => {

            next(error, resultado)
        })
}

function createAlmacen(almacen, next) {
    AlmacenModel
        .query(`INSERT INTO almacen 
                SET ?`, almacen, (error, resultado, fields) => {

            next(error)
        })
}

function createAlmacenes(almacenes, next){
    AlmacenModel
        .query(`INSERT INTO almacen ( idSucursal, idProducto ) 
                VALUES ?`, [almacenes], (error, resultado, fields) => {

            next(error)
        })
}

function updateAlmacen(almacen, next) {
    AlmacenModel
        .query(`UPDATE almacen 
                SET ? 
                WHERE id = ?`, [almacen,almacen.id], (error, resultado, fields) => {

            next(error)
        })
}

module.exports = {
    getAlmacen,
    getAlmacenById,
    getAlmacenByCategory,
    getAlmacenBySucursalAndProduct,
    getConsumoById,
    getConsumoByCategory,
    getAlmacenByPlazaAndCategory,
    getAlmacenBySucursalAndCategory,
    getConsumoByPlazaAndCategory,
    getConsumoBySucursalAndCategory,
    createAlmacen,
    createAlmacenes,
    updateAlmacen
}