/**
 * Created by Jorge Mejicanos
 */
'use strict'

const TipoModel = require('./coneccion')

function getTipoById(id, next) {
    TipoModel
        .query(`SELECT * 
                FROM tipos s 
                WHERE s.id = ?`, id, (error, resultado, fields) => {

                next(error, resultado[0])
            })
}

function getTipoByPlaza(plaza, next) {
    TipoModel
        .query(`select t.id, t.nombre, t.descripcion
                from tipos t 
                join sucursales s on t.id = s.idTipo AND s.plaza = ?`, 
                plaza, (error, resultado, fields) => {
                try { // si suerge un error es de la base de datos
                    next(error, resultado[0].id)
                } catch (error) { // si no se encontro tipo
                    next(error = null, 0)
                }
            })
}

function getIdTipoByNombre(nombre, next) {
    TipoModel
        .query(`SELECT s.id 
                FROM tipos s 
                WHERE s.nombre = ?`, nombre, (error, resultado, fields) => {
                try { // si suerge un error es de la base de datos
                    next(error, resultado[0].id)
                } catch (error) { // si no se encontro sucursal
                    next(error = null, 0)
                }
            })
}

function getNamesOfTipos(next) {
    TipoModel
        .query(`SELECT t.nombre 
                FROM tipos t`, (error, resultado, fields) => {

                next(error, resultado)
            })
}

function getTipos(next) {
    TipoModel
        .query(`SELECT * 
                FROM tipos`, (error, resultado, fields) => {

                next(error, resultado)
            })
}

function createTipo(tipo, next) {
    TipoModel
        .query(`INSERT INTO tipos 
                SET ?`, tipo, (error, resultado, fields) => {

                next(error)
            })
}

function updateTipo(tipo, next) {
    TipoModel
        .query(`UPDATE tipos s 
                SET ? 
                WHERE s.id = ?`, [tipo, tipo.id], (error, resultado, fields) => {

                next(error)
            })
}

function deleteTipo(id, next) {
    TipoModel
        .query(`DELETE FROM tipos 
                WHERE id = ? `, id, (error, resultado, fields) => {

                next(error)
            })
}

module.exports = {
    getTipoById,
    getIdTipoByNombre,
    getNamesOfTipos,
    getTipoByPlaza,
    getTipos,
    createTipo,
    updateTipo,
    deleteTipo
}