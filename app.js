/**
 * Created by Jorge Mejicanos
 */
'use strict'

const express = require('express'),
    bodyParser = require('body-parser'),
    pug = require('pug'),
    restFul = require('method-override')('_method'),
    config = require('./config'),
    cookieSession = require("cookie-session"),
    Index = require('./controllers/index'),
    Sucursal_router = require('./routes/sucursal'),
    Tipo_router = require('./routes/tipo'),
    Category_router = require('./routes/categoria'),
    Router_product = require('./routes/producto'),
    Almacen_router = require('./routes/almacen'),
    Distribucion_router = require('./routes/distribucion'),
    Distribuido_router = require('./routes/distribuido'),
    session_admin = require('./middleware/session-admin'),
    session_general_admin = require('./middleware/session-general-admin'),
    session_active = require('./middleware/session-active'),
    session_active_sucursal = require('./middleware/session-active-sucursal'),
    app = express()

app
    // configuracion app
    .set("view engine", "pug")
    .set('views', config.VIEWS)
    .set('port', config.PORT)
    // ejecutando middleware
    .use(config.PUBLIC, express.static('public'))
    // parse application/json
    .use(bodyParser.json())
    // parse applicaction/x-www-form-urlencoded
    .use(bodyParser.urlencoded({ extended: false }))
    // para put y delete
    .use(restFul)
    .use(cookieSession({
        name: "session",
        keys: ["inventario", "umg"]
    }))
// inventario/
app
    .get('/', Index.index)
    .get('/login', Index.loginGet)
    .post('/login', Index.loginPost)
    .get('/logout', Index.logout)
// inventario/almacen
app
    .use("/almacen", session_active)
    .use("/almacen", Almacen_router)
// inventario/distribucion
app
    .use("/distribucion", session_active)
    .use("/distribucion", Distribucion_router)
// inventario/distribuido
app
    .use("/distribuido", session_active)
    .use("/distribuido", Distribuido_router)
// inventario/sucursales
app
    .use("/sucursales", session_active)
    .use('/sucursales', Sucursal_router)
// inventario/tipos
app
    .use("/tipos", session_active)
    .use('/tipos', Tipo_router)
// inventario/categories
app
    .use("/categories", session_active)
    .use('/categories', Category_router)
// inventario/products
app
    .use("/products", session_active)
    .use("/products", Router_product)
    // para error 404
    .use(Index.error404)

module.exports = app