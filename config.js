/**
 * Created by Jorge Mejicanos
 */
module.exports = {
    PORT : 5000,
    PUBLIC : '/public',
    VIEWS: './views'
}